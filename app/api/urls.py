from django.urls import path, include
from rest_framework import routers
from .viewsets import *

router = routers.DefaultRouter()
router.register("evento", EventoViewSet)
router.register("compito", CompitoViewSet)
router.register("classe", ClasseViewSet)
router.register("alunno", AlunnoViewSet)
router.register("genitore", GenitoreViewSet)
router.register("insegnante", InsegnanteViewSet)
router.register("user", UserViewSet)
# router.register(r'users', UserViewSet)
urlpatterns = router.urls

urlpatterns += [path("rest-auth/", include("rest_auth.urls"))]
