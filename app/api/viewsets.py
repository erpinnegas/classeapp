from rest_framework.authentication import (
    SessionAuthentication,
    BasicAuthentication,
    TokenAuthentication,
)
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import get_object_or_404
from rest_framework import generics, viewsets

from .serializers import *
from core.models import *
from core.views import is_genitore, is_insegnante


class APIQualcunoRequiredMixin:
    authentication_classes = (
        SessionAuthentication,
        BasicAuthentication,
        TokenAuthentication,
    )
    permission_classes = (IsAuthenticated,)

    def test_func(self):
        user = self.request.user
        return is_genitore(user) or is_insegnante(user)


class EventoViewSet(APIQualcunoRequiredMixin, viewsets.ModelViewSet):
    queryset = Evento.objects.all()
    serializer_class = EventoSerializer


class CompitoViewSet(APIQualcunoRequiredMixin, viewsets.ModelViewSet):
    queryset = Compito.objects.all()
    serializer_class = CompitoSerializer


class ClasseViewSet(APIQualcunoRequiredMixin, viewsets.ModelViewSet):
    queryset = Classe.objects.all()
    serializer_class = ClasseSerializer


class AlunnoViewSet(APIQualcunoRequiredMixin, viewsets.ModelViewSet):
    queryset = Alunno.objects.all()
    serializer_class = AlunnoSerializer


class GenitoreViewSet(APIQualcunoRequiredMixin, viewsets.ModelViewSet):
    queryset = Genitore.objects.all()
    serializer_class = GenitoreSerializer


class InsegnanteViewSet(APIQualcunoRequiredMixin, viewsets.ModelViewSet):
    queryset = Insegnante.objects.all()
    serializer_class = InsegnanteSerializer


class UserViewSet(APIQualcunoRequiredMixin, viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()


# class ClasseListCreate(generics.ListCreateAPIView):
#     queryset = Classe.objects.all()
#     serializer_class = ClasseSerializer
