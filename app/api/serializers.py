from rest_framework import serializers
from core.models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "first_name", "last_name", "email")


class InsegnanteSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Insegnante
        fields = "__all__"
        depth = 1


class GenitoreSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Genitore
        fields = "__all__"
        depth = 1


class AlunnoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alunno
        fields = "__all__"
        depth = 1


class ClasseSerializer(serializers.ModelSerializer):
    insegnanti = InsegnanteSerializer(many=True)

    class Meta:
        model = Classe
        fields = "__all__"
        depth = 1


class CompitoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Compito
        fields = "__all__"
        depth = 1


class EventoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Evento
        fields = "__all__"
        depth = 1
