from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms.models import inlineformset_factory

from core.models import *


class GenitoreForm(forms.ModelForm):
    class Meta:
        model = Genitore
        exclude = ["user", "figli"]


class AlunnoForm(forms.ModelForm):
    # classe = forms.UUIDField(label="Codice invito classe")

    # def clean_classe(self):
    #     """ Controlla che l'uuid della classe esista """
    #     uuid = self.cleaned_data["classe"]
    #     try:
    #         classe = Classe.objects.get(uuid=uuid)
    #     except Classe.DoesNotExist:
    #         raise ValidationError("Codice classe non esistente")

    class Meta:
        model = Alunno
        exclude = ["genitore"]
        help_texts = {
            "classe": "Il codice di invito e' una sequenza di caratteri "
            + "utilizzata per accedere a una classe. Se non ne sei "
            + "in possesso, chiedi al tuo rappresentante di classe."
        }


class EventoForm(forms.ModelForm):
    class Meta:
        model = Evento
        exclude = ["classe", "creatore"]


ClasseAlunnoFormset = forms.inlineformset_factory(
    Classe, Alunno, form=AlunnoForm, extra=1, can_delete=False
)


class SceltaFiglioForm(forms.Form):

    figlio = forms.ModelChoiceField(queryset=None, required=False)

    def __init__(self, *args, **kwargs):
        genitore = kwargs.pop("genitore")
        super(SceltaFiglioForm, self).__init__(*args, **kwargs)
        self.fields["figlio"].queryset = genitore.figli.all()


class RicercaClasseForm(forms.Form):
    classe = forms.UUIDField(label="Codice invito classe")
