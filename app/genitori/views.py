from django.db import transaction
from django.forms import modelform_factory
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, Http404, JsonResponse
from django.urls import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth import views as auth_views, authenticate, login
from django.views.generic import *
from django import views
from itertools import zip_longest

from django.utils import timezone

from .forms import *
from core.forms import CustomUserCreationForm, AuthenticationFormGenitore
from core.models import *
from core.views import is_genitore

LOGIN_URL_GENITORE = reverse_lazy("genitori:login-genitore")
# Create your views here.


class GenitoreRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):
    """ Classe da comporre (aggiungere in testa alla lista di classi di 
        eredita') per ottenere una View che autorizza solo i genitori 
        autenticati.
    """

    login_url = LOGIN_URL_GENITORE

    def test_func(self):
        return hasattr(self.request.user, "genitore")


class AjaxableResponseMixin:
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super().form_valid(form)
        if self.request.is_ajax():
            data = {"pk": self.object.pk}
            return JsonResponse(data)
        else:
            return response


def genitore_required(view):
    def wrapper(request, *args, **kwargs):
        if not is_genitore(request.user):
            raise PermissionDenied
        return view(request, *args, **kwargs)

    return wrapper


def ajax_required(view):
    def wrapper(request, *args, **kwargs):
        if not request.is_ajax():
            raise PermissionDenied
        return view(request, *args, **kwargs)

    return wrapper


class SignupGenitore(CreateView):
    form_class = CustomUserCreationForm
    template_name = "genitori/signup_genitore.html"

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
            genitore = Genitore.objects.create(user=self.object)
            genitore.save()
        return redirect(LOGIN_URL_GENITORE)


class LoginGenitore(views.View):
    def get(self, request):
        form = AuthenticationFormGenitore()
        return render(request, "genitori/login_genitore.html", {"form": form})

    def post(self, request):
        form = AuthenticationFormGenitore(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("genitori:home-genitore")
        return render(request, "genitori/login_genitore.html", {"form": form})


class ClasseCreate(GenitoreRequiredMixin, CreateView):
    model = Classe
    fields = ["anno", "sezione", "anno_inizio"]
    template_name = "genitori/classe_create.html"

    def get_context_data(self, **kwargs):
        data = super(ClasseCreate, self).get_context_data(**kwargs)
        genitore = self.request.user.genitore
        if self.request.POST:
            data["scelta_figlio"] = SceltaFiglioForm(
                self.request.POST, genitore=genitore
            )
            data["form_alunni"] = ClasseAlunnoFormset(self.request.POST)
        else:
            form = data["form"]
            data["scelta_figlio"] = SceltaFiglioForm(genitore=genitore)
            data["form_alunni"] = ClasseAlunnoFormset()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        form_alunni = context["form_alunni"]
        scelta_figlio = context["scelta_figlio"]
        genitore = self.request.user.genitore
        with transaction.atomic():
            classe = form.save()
            classe.rappresentanti.add(genitore)
            classe.save()
            self.object = classe
            if scelta_figlio.is_valid() and scelta_figlio.cleaned_data["figlio"]:
                alunno = scelta_figlio.cleaned_data["figlio"]
                alunno.classe = self.object
                alunno.save()
            elif form_alunni.is_valid():
                alunni = form_alunni.save(commit=False)
                for alunno in alunni:
                    alunno.genitore = genitore
                    alunno.classe = self.object
                    alunno.save()
            classe.save()
        return super(ClasseCreate, self).form_valid(form)


class ClasseDelete(GenitoreRequiredMixin, DeleteView):
    model = Classe
    template_name = "genitori/classe_delete.html"
    success_url = "/"

    def get_object(self, queryset=None):
        classe = super(ClasseDelete, self).get_object(queryset)
        if not self.request.user.genitore.is_rappresentante(classe):
            raise PermissionDenied
        return classe


@genitore_required
@ajax_required
def ajax_mostra_figlio_form(request):
    if request.method == "GET":
        form = AlunnoForm()
        return render(request, "genitori/figlio_form.html", {"form": form})


class AggiungiFiglio(GenitoreRequiredMixin, FormView):
    form_class = AlunnoForm
    template_name = "genitori/aggiungi_figlio.html"

    def form_valid(self, form):
        with transaction.atomic():
            alunno = form.save()
            request.user.genitore.figli.add(alunno)
        return redirect("/")


class AggiungiFiglio(GenitoreRequiredMixin, views.View):
    def get(self, request):
        form = AlunnoForm()
        return render(request, "genitori/aggiungi_figlio.html", {"form": form})

    def post(self, request):
        form = AlunnoForm(request.POST)
        if form.is_valid():
            with transaction.atomic():
                alunno = form.save()
                request.user.genitore.figli.add(alunno)
            return redirect("/")
        else:
            return render(request, "genitori/aggiungi_figlio.html", {"form": form})


class EventoCreate(GenitoreRequiredMixin, CreateView):
    template_name = "genitori/evento_create.html"
    model = Evento
    form_class = EventoForm

    def dispatch(self, request, *args, **kwargs):
        self.classe = get_object_or_404(Classe, pk=self.kwargs.get("classe_pk", None))
        return super().dispatch(request, *args, **kwargs)

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        form.fields["invitati"].queryset = Genitore.objects.filter(
            figli__classe=self.classe
        ).exclude(user=self.request.user)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["classe"] = self.classe
        return context

    def form_valid(self, form):
        genitore = self.request.user.genitore
        with transaction.atomic():
            evento = form.save(commit=False)
            evento.classe = self.classe
            evento.creatore = genitore
            evento.save()
            form.save_m2m()
            evento.invitati.add(genitore, through_defaults={"conferma": True})
            evento.save()

        return redirect(reverse("dettagli-classe", kwargs={"pk": self.classe.pk}))


class EventoDetail(GenitoreRequiredMixin, DetailView):
    template_name = "genitori/evento_dettagli.html"
    model = Evento
    context_object_name = "evento"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        evento = self.get_object()
        context["inviti"] = list(
            zip_longest(
                InvitoEvento.objects.all()
                .filter(evento=evento)
                .order_by("genitore_id"),
                InvitoColletta.objects.all()
                .filter(colletta__evento=evento)
                .order_by("genitore_id"),
            )
        )
        genitore = self.request.user.genitore
        context["invito_evento"] = evento.inviti.get(genitore=genitore)
        if hasattr(evento, "colletta"):
            context["invito_colletta"] = evento.colletta.inviti.all().get(
                genitore=genitore
            )
        return context


class EventoUpdate(GenitoreRequiredMixin, UpdateView):
    model = Evento
    template_name = "genitori/evento_update.html"
    context_object_name = "evento"
    fields = ("tipo", "nome", "descrizione", "data", "luogo_str")

    def get_object(self, queryset=None):
        evento = super().get_object(queryset)
        if not evento.creatore == self.request.user.genitore:
            raise PermissionDenied
        return evento


class EventoInvitoUpdate(GenitoreRequiredMixin, AjaxableResponseMixin, UpdateView):
    """
    View da usare come ajax per mostrare un form di aggiunta invitati.
    """

    model = Evento
    fields = ["invitati"]
    context_object_name = "evento"

    def get_object(self, queryset=None):
        evento = super().get_object(queryset)
        if not evento.creatore == self.request.user.genitore:
            raise PermissionDenied
        return evento

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        invitati = form.fields["invitati"]
        classe = self.object.classe
        invitati.queryset = (
            Genitore.objects.all()
            .filter(figli__classe=classe)
            .exclude(user=self.request.user)
        )
        return form

    def form_valid(self, form):
        response = super().form_valid(form)
        genitore = self.request.user.genitore
        with transaction.atomic():
            evento = form.save()
            evento.invitati.add(genitore, through_defaults={"conferma": True})
            if hasattr(evento, "colletta"):
                evento.colletta.invitati.set(evento.invitati.all())
        return response


@genitore_required
@ajax_required
def ajax_accetta_rifiuta_invito_evento(request, pk, accetta):
    """
    Richiesta AJAX invocata alla pressione del bottone accetta/rifiuta invito ad un evento,

    """
    if request.method == "POST":
        raise PermissionDenied
    invito = InvitoEvento.objects.get(pk=pk)
    if not invito.genitore == request.user.genitore:
        raise PermissionDenied
    # aggiorna la scelta
    if accetta in "yn":
        invito.conferma = accetta == "y" or not accetta == "n"
        invito.save()
        return render(
            request, "genitori/bottoni_evento.html", context={"invito": invito}
        )
    else:
        return JsonResponse({"error": "Argomento non valido"}, status=403)


@ajax_required
@genitore_required
def ajax_accetta_rifiuta_invito_colletta(request, pk, accetta):
    """
    Richiesta AJAX invocata alla pressione del bottone accetta/rifiuta invito 
    ad un colletta
    """
    invito = InvitoColletta.objects.get(pk=pk)
    if not invito.genitore == request.user.genitore:
        raise PermissionDenied
    if accetta in "yn":
        invito.conferma = accetta == "y" or not accetta == "n"
        invito.save()
        return render(
            request, "genitori/bottoni_colletta.html", context={"invito": invito}
        )
    else:
        return JsonResponse({"error": "Argomento non valido"}, status=403)


class ClasseAggiungiGenitore(GenitoreRequiredMixin, views.View):

    FiglioForm = modelform_factory(Alunno, fields=("nome", "cognome"))

    def post(self, request, uuid):
        context = self.get_context(request, uuid)
        form_figlio = context["form_figlio"]
        form_scelta_figlio = context["form_scelta_figlio"]
        genitore = self.request.user.genitore
        classe = context["classe"]
        if form_figlio.is_valid():
            with transaction.atomic():
                figlio = form_figlio.save(commit=False)
                figlio.genitore = genitore
                figlio.classe = classe
                figlio.save()
        elif form_scelta_figlio.is_valid():
            with transaction.atomic():
                figlio = form_figlio.cleaned_data["figlio"]
                figlio.genitore = genitore
                figlio.classe = context["classe"]
                figlio.save()
        else:  # errori nella validazione di entrambi i form
            return render(request, "genitori/conferma_invito.html", context=context)
        return redirect(reverse("dettagli-classe", kwargs={"pk": classe.pk}))

    def get(self, request, uuid):
        context = self.get_context(request, uuid)
        return render(request, "genitori/conferma_invito.html", context=context)

    def get_context(self, request, uuid):
        classe = get_object_or_404(Classe, uuid=uuid)
        if self.request.POST:
            form_figlio = self.FiglioForm(request.POST)
            form_scelta_figlio = SceltaFiglioForm(
                request.POST, genitore=request.user.genitore
            )
        else:
            form_scelta_figlio = SceltaFiglioForm(genitore=request.user.genitore)
            form_figlio = self.FiglioForm()
        return {
            "classe": classe,
            "classe_uuid": str(classe.uuid),
            "form_scelta_figlio": form_scelta_figlio,
            "form_figlio": form_figlio,
        }


# class CollettaCreate(GenitoreRequiredMixin, CreateView):
#     model = Colletta
#     fields = ("nome", "descrizione", "data", "quota_consigliata", "invitati", "evento")
#     template_name = "genitori/colletta_create.html"


class CollettaAssociate(GenitoreRequiredMixin, CreateView):
    model = Colletta
    fields = ("nome", "descrizione", "quota_consigliata", "invitati")
    template_name = "genitori/colletta_create.html"

    def dispatch(self, request, *args, **kwargs):
        self.evento = Evento.objects.get(pk=self.kwargs["evento_pk"])
        return super(CollettaAssociate, self).dispatch(request, *args, **kwargs)

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        form.fields["invitati"].queryset = self.evento.invitati.all().exclude(
            user=self.request.user
        )
        form.fields["invitati"].initial = self.evento.invitati.all()
        return form

    def form_valid(self, form):
        with transaction.atomic():

            colletta = form.save(commit=False)
            colletta.evento = self.evento
            colletta.creatore = self.request.user.genitore
            colletta.save()
            form.save_m2m()
            colletta.invitati.add(
                self.request.user.genitore, through_defaults={"conferma": True}
            )
            colletta.save()
            self.object = colletta
            assert self.object.invitati.all().filter(user=self.request.user).exists()
            return redirect(self.get_success_url())
        return super().form_invalid(form)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["nome_evento"] = self.evento.nome
        return context


class CollettaUpdate(GenitoreRequiredMixin, UpdateView):
    model = Colletta
    fields = ("nome", "descrizione", "quota_consigliata")
    template_name = "genitori/colletta_update.html"

    def get_object(self, queryset=None):
        colletta = super().get_object(queryset)
        if not colletta.creatore == self.request.user.genitore:
            raise PermissionDenied
        return colletta


class ModificaRappresentanti(GenitoreRequiredMixin, UpdateView):
    model = Classe
    fields = ["rappresentanti"]
    template_name = "genitori/classe_update.html"

    def get_object(self, queryset=None):
        classe = super(ClasseDelete, self).get_object(queryset)
        if not self.request.user.genitore.is_rappresentante(classe):
            raise PermissionDenied
        return classe

    def get_form(self, **kwargs):
        """ Rimuovere l'utente attivo dal form di modifica per evitare l'accidentale
            auto-rimozione di un rappresentante """
        form = super().get_form(**kwargs)
        field = form.fields["rappresentanti"]
        field.queryset = field.queryset.exclude(user=self.request.user)
        return form

    def form_valid(self, form):
        """ Assicurarsi che il rappresentante attuale venga comunque incluso"""
        response = super().form_valid(form)
        self.object.rappresentanti.add(self.request.user.genitore)
        self.object.save()
        return response
