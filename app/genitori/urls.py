"""
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import *
from core.views import home_view

urlpatterns = [
    path("signup/genitore/", SignupGenitore.as_view(), name="signup-genitore"),
    path("login/genitore/", LoginGenitore.as_view(), name="login-genitore"),
    path("profile/genitore/", home_view, name="home-genitore"),
    path("classe/create", ClasseCreate.as_view(), name="aggiungi-classe"),
    path("classe/<int:pk>/delete", ClasseDelete.as_view(), name="classe-delete"),
    path(
        "classe/<uuid:uuid>/conferma-genitore",
        ClasseAggiungiGenitore.as_view(),
        name="classe_aggiungi_genitore",
    ),
    path(
        "classe/<int:pk>/modifica_rappresentanti",
        ModificaRappresentanti.as_view(),
        name="modifica_rappresentanti",
    ),
    # eventi
    path(
        "genitore/classe/<int:classe_pk>/aggiungi-evento",
        EventoCreate.as_view(),
        name="aggiungi-evento",
    ),
    path("evento/<int:pk>/", EventoDetail.as_view(), name="dettagli-evento"),
    path("evento/<int:pk>/update", EventoUpdate.as_view(), name="evento-update"),
    # inviti a eventi
    path(
        "evento/<int:pk>/inviti_edit",
        EventoInvitoUpdate.as_view(template_name="genitori/evento_invito_form.html"),
        name="aggiungi-invito-evento",
    ),
    path(
        "invito_evento/<int:pk>/<str:accetta>/",
        ajax_accetta_rifiuta_invito_evento,
        name="conferma-invito-evento",
    ),
    # collette
    # path("colletta/create", CollettaCreate.as_view(), name="colletta-create"),
    path(
        "evento/<int:evento_pk>/colletta/create",
        CollettaAssociate.as_view(),
        name="colletta-evento-associa",
    ),
    path(
        "evento/<int:pk>/colletta/update",
        CollettaUpdate.as_view(),
        name="colletta-update",
    ),
    # path("colletta/<int:pk>", CollettaDetail.as_view(), name="colletta-detail"),
    path(
        "invito_colletta/<int:pk>/<str:accetta>/",
        ajax_accetta_rifiuta_invito_colletta,
        name="conferma-invito-colletta",
    ),
]
