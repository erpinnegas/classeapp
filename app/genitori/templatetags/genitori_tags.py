from django import template

register = template.Library()


@register.simple_tag
def genitore_is_rappresentante(genitore, classe):
    return genitore.is_rappresentante(classe)
