# Generated by Django 2.2.2 on 2019-06-23 16:10

import core.managers
import core.models
import datetime
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('first_name', models.CharField(max_length=50, verbose_name='Nome')),
                ('last_name', models.CharField(max_length=100, verbose_name='Cognome')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='Indirizzo email')),
                ('data_nascita', models.DateField(blank=True, null=True, verbose_name='Data di nascita')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_genitore', models.BooleanField(default=False)),
                ('is_insegnante', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', core.managers.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Classe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('anno', models.PositiveSmallIntegerField(validators=[django.core.validators.MaxValueValidator(5)])),
                ('sezione', models.CharField(max_length=5)),
                ('anno_inizio', models.PositiveSmallIntegerField(default=core.models.current_year, validators=[django.core.validators.MinValueValidator(2005), django.core.validators.MaxValueValidator(core.models.current_year)])),
            ],
        ),
        migrations.CreateModel(
            name='Colletta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=200)),
                ('descrizione', models.TextField(blank=True, default='')),
                ('data', models.DateField(default=datetime.datetime.today)),
                ('quota_consigliata', models.DecimalField(decimal_places=2, max_digits=6)),
            ],
        ),
        migrations.CreateModel(
            name='Evento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(choices=[('GI', 'Gita'), ('CO', 'Compleanno')], max_length=2)),
                ('nome', models.CharField(max_length=200)),
                ('descrizione', models.TextField(blank=True, default='')),
                ('data', models.DateField(default=datetime.datetime.today)),
                ('luogo_str', models.CharField(max_length=300, verbose_name="Luogo dell'evento")),
                ('classe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='eventi', to='core.Classe')),
            ],
        ),
        migrations.CreateModel(
            name='Genitore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Insegnante',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='RichiestaInsegnante',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('accettata', models.BooleanField(blank=True, default=False)),
                ('data_creata', models.DateTimeField(default=django.utils.timezone.now)),
                ('data_accettata', models.DateTimeField(null=True)),
                ('classe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='richieste_partecipazione_insegnanti', to='core.Classe')),
                ('insegnante', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='richieste_partecipazione', to='core.Insegnante')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RichiestaGenitore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('accettata', models.BooleanField(blank=True, default=False)),
                ('data_creata', models.DateTimeField(default=django.utils.timezone.now)),
                ('data_accettata', models.DateTimeField(null=True)),
                ('classe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='richieste_partecipazione_genitori', to='core.Classe')),
                ('genitore', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='richieste_partecipazione', to='core.Genitore')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='InvitoEvento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('conferma', models.BooleanField(default=False)),
                ('evento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='inviti', to='core.Evento')),
                ('genitore', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='inviti', to='core.Genitore')),
            ],
        ),
        migrations.CreateModel(
            name='InvitoColletta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('conferma', models.BooleanField(default=False)),
                ('colletta', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='inviti', to='core.Colletta')),
                ('genitore', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='inviti_collette', to='core.Genitore')),
            ],
        ),
        migrations.AddField(
            model_name='evento',
            name='creatore',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='eventi_creati', to='core.Genitore'),
        ),
        migrations.AddField(
            model_name='evento',
            name='invitati',
            field=models.ManyToManyField(blank=True, through='core.InvitoEvento', to='core.Genitore'),
        ),
        migrations.CreateModel(
            name='Compito',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('materia', models.CharField(max_length=100)),
                ('data', models.DateField(default=datetime.datetime.today)),
                ('testo', models.TextField()),
                ('foto', models.ImageField(blank=True, null=True, upload_to='compiti')),
                ('classe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='compiti', to='core.Classe')),
            ],
        ),
        migrations.AddField(
            model_name='colletta',
            name='creatore',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='collette_create', to='core.Genitore'),
        ),
        migrations.AddField(
            model_name='colletta',
            name='evento',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='colletta', to='core.Evento'),
        ),
        migrations.AddField(
            model_name='colletta',
            name='invitati',
            field=models.ManyToManyField(through='core.InvitoColletta', to='core.Genitore'),
        ),
        migrations.AddField(
            model_name='classe',
            name='insegnanti',
            field=models.ManyToManyField(related_name='classi', to='core.Insegnante'),
        ),
        migrations.AddField(
            model_name='classe',
            name='rappresentanti',
            field=models.ManyToManyField(related_name='classi_rappresentanza', to='core.Genitore'),
        ),
        migrations.CreateModel(
            name='Alunno',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100)),
                ('cognome', models.CharField(max_length=100)),
                ('classe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='alunni', to='core.Classe')),
                ('genitore', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='figli', to='core.Genitore')),
            ],
        ),
    ]
