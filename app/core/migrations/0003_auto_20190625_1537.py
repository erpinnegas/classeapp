# Generated by Django 2.2.2 on 2019-06-25 15:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20190625_1254'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='is_genitore',
        ),
        migrations.RemoveField(
            model_name='user',
            name='is_insegnante',
        ),
        migrations.AlterField(
            model_name='colletta',
            name='evento',
            field=models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='colletta', to='core.Evento'),
            preserve_default=False,
        ),
    ]
