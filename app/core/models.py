import uuid
from django.db import models
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.shortcuts import reverse

from datetime import datetime

from .managers import UserManager

# Create your models here.


def current_year():
    return datetime.now().year


class User(AbstractUser):
    """ Istanza base di un utente necessaria all'autenticazione
    """

    username = None
    first_name = models.CharField("Nome", max_length=50)
    last_name = models.CharField("Cognome", max_length=100)
    email = models.EmailField("Indirizzo email", unique=True)  # chiave per il login
    data_nascita = models.DateField("Data di nascita", null=True, blank=True)

    date_joined = models.DateTimeField(default=timezone.now)

    REQUIRED_FIELDS = []

    USERNAME_FIELD = "email"
    objects = UserManager()

    def full_name(self):
        return "{nome} {cognome}".format(
            nome=str(self.first_name), cognome=str(self.last_name)
        ).title()


class Genitore(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def is_rappresentante(self, classe):
        return self in classe.rappresentanti.all()

    def full_name(self):
        return self.user.full_name()

    def __str__(self):
        return "{} ({})".format(
            str(self.user.full_name()),
            ",".join([str(figlio.full_name()) for figlio in self.figli.all()]),
        ).title()


class Insegnante(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.user.full_name())

    def full_name(self):
        return self.user.full_name()


class Alunno(models.Model):
    nome = models.CharField(max_length=100, blank=False)
    cognome = models.CharField(max_length=100, blank=False)

    genitore = models.ForeignKey(
        "Genitore", on_delete=models.CASCADE, related_name="figli"
    )

    classe = models.ForeignKey(
        "Classe", on_delete=models.CASCADE, related_name="alunni"
    )

    def full_name(self):
        return "{nome} {cognome}".format(nome=str(self.nome), cognome=str(self.cognome))

    def __str__(self):
        return "{nome} {cognome} (figlio di {genitore})".format(
            nome=str(self.nome),
            cognome=str(self.cognome),
            genitore=self.genitore.full_name(),
        ).title()


class Classe(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    anno = models.PositiveSmallIntegerField(validators=[MaxValueValidator(5)])
    sezione = models.CharField(max_length=5, blank=False)
    anno_inizio = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2005), MaxValueValidator(current_year)],
        default=current_year,
    )
    insegnanti = models.ManyToManyField("Insegnante", related_name="classi")
    rappresentanti = models.ManyToManyField(
        "Genitore", related_name="classi_rappresentanza", blank=True
    )

    def get_genitori(self):
        return (alunno.genitore for alunno in self.alunni)

    def get_absolute_url(self):
        return reverse("dettagli-classe", kwargs={"pk": self.pk})

    def __str__(self):
        return "{}^{}".format(str(self.anno), self.sezione)

    def __unicode__(self):
        return str(self)


class Compito(models.Model):
    """ Un'unita' di informazioni che l'insegnante di una classe fissa
        sul calendario affinche' i genitori possano controllarlo.
    """

    creatore = models.ForeignKey(
        "Insegnante", on_delete=models.CASCADE, related_name="compiti_creati", null=True
    )
    materia = models.CharField(max_length=100)
    classe = models.ForeignKey(
        "Classe", on_delete=models.CASCADE, related_name="compiti"
    )
    data = models.DateField(default=datetime.today)
    testo = models.TextField(blank=False)
    foto = models.ImageField(upload_to="compiti", null=True, blank=True)

    def __str__(self):
        return "{} {} {}".format(str(self.materia), str(self.data), str(self.classe))

    def __unicode__(self):
        return str(self)


class Evento(models.Model):
    """
        Avvenimento di vario tipo che viene fissato sul calendario e mantiene 
        l'informazione di chi e' stato invitato e chi conferma la propria 
        partecipazione.
    """

    GITA = "GI"
    COMPLEANNO = "CO"

    TIPO_EVENTO = ((GITA, "Gita"), (COMPLEANNO, "Compleanno"))

    tipo = models.CharField(max_length=2, choices=TIPO_EVENTO, null=False)

    nome = models.CharField(max_length=200)
    descrizione = models.TextField(blank=True, default="")
    data = models.DateField(default=datetime.today)

    luogo_str = models.CharField("Luogo dell'evento", max_length=300)

    creatore = models.ForeignKey(
        "Genitore", related_name="eventi_creati", on_delete=models.CASCADE
    )
    invitati = models.ManyToManyField("Genitore", through="InvitoEvento", blank=True)
    classe = models.ForeignKey(
        "Classe", related_name="eventi", on_delete=models.CASCADE
    )

    def get_absolute_url(self):
        return reverse("genitori:dettagli-evento", kwargs={"pk": self.pk})

    def __str__(self):
        return str(self.nome)

    def __unicode__(self):
        return str(self.nome)


class InvitoEvento(models.Model):
    evento = models.ForeignKey(
        "Evento", on_delete=models.CASCADE, related_name="inviti"
    )
    genitore = models.ForeignKey(
        "Genitore", on_delete=models.CASCADE, related_name="inviti"
    )
    conferma = models.BooleanField(default=False)

    def __str__(self):
        return "{} invitato a {} ({})".format(
            str(self.genitore), str(self.evento), str(self.conferma)
        )


class Colletta(models.Model):
    """
        Una raccolta di fondi per organizzarsi a sostenere la spesa relativa
        ad un evento.
    """

    nome = models.CharField(max_length=200)
    descrizione = models.TextField(blank=True, default="")

    quota_consigliata = models.DecimalField(max_digits=6, decimal_places=2)

    invitati = models.ManyToManyField("Genitore", blank=True, through="InvitoColletta")

    evento = models.OneToOneField(
        "Evento", related_name="colletta", on_delete=models.CASCADE
    )

    creatore = models.OneToOneField(
        "Genitore", related_name="collette_create", on_delete=models.CASCADE
    )

    def __str__(self):
        return "{} (associata a {})".format(self.nome, self.evento)

    def get_absolute_url(self):
        return reverse("genitori:dettagli-evento", kwargs={"pk": self.evento.pk})


class InvitoColletta(models.Model):
    colletta = models.ForeignKey(
        "Colletta", on_delete=models.CASCADE, related_name="inviti"
    )
    genitore = models.ForeignKey(
        "Genitore", on_delete=models.CASCADE, related_name="inviti_collette"
    )
    conferma = models.BooleanField(default=False)

    def __str__(self):
        return "{} invitato a {} ({})".format(
            str(self.genitore.full_name()), str(self.colletta), str(self.conferma)
        )
