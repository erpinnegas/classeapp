from django.contrib.auth.forms import AuthenticationForm
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.shortcuts import render, redirect, get_object_or_404
from django.views import View
from django.views.generic import (
    CreateView,
    FormView,
    RedirectView,
    DetailView,
    UpdateView,
)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse
from .models import *
from .forms import UuidForm, CustomUserChangeForm

# Create your views here.


def is_genitore(user):
    return hasattr(user, "genitore")


def is_insegnante(user):
    return hasattr(user, "insegnante")


class QualcunoRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):

    login_url = "/"

    def test_func(self):
        user = self.request.user
        return is_genitore(user) or is_insegnante(user)


def home_view(request):
    if request.user.is_authenticated:
        form_ricerca_classe = UuidForm()
        context = {"form_ricerca_classe": form_ricerca_classe}
        if is_insegnante(request.user):
            return render(request, "insegnanti/home_insegnante.html", context=context)
        if is_genitore(request.user):
            return render(request, "genitori/home_genitore.html", context=context)
    return render(request, "core/home.html")


def logout(request):
    auth.logout(request)
    return redirect("/")


class LogoutView(RedirectView):
    """
    Provides users the ability to logout
    """

    url = "/"

    def get(self, request, *args, **kwargs):
        auth.logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    form_class = CustomUserChangeForm
    template_name = "core/user_update.html"
    success_url = "/"

    def get_object(self, queryset=None):
        return self.request.user


@login_required
def invito_classe(request, uuid):
    user = request.user
    classe = get_object_or_404(Classe, uuid=uuid)
    if is_insegnante(user):
        return redirect(
            reverse("insegnanti:classe_aggiungi_insegnante", kwargs={"uuid": str(uuid)})
        )
    elif is_genitore(user):
        return redirect(
            reverse("genitori:classe_aggiungi_genitore", kwargs={"uuid": str(uuid)})
        )
    else:
        raise PermissionDenied


@login_required
def ricerca_classe(request):
    if request.method == "POST":
        uuid_form = UuidForm(request.POST)
        if uuid_form.is_valid():
            uuid = uuid_form.cleaned_data["uuid"]
            classe = get_object_or_404(Classe, uuid=uuid)
            return invito_classe(request, uuid)
    return redirect("/")


class ClasseDetail(QualcunoRequiredMixin, DetailView):
    """ View unica per permettere la get_absolute_url sul modello Classe
    """

    template_name = "core/classe_dettagli.html"
    model = Classe
    context_object_name = "classe"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        oggi = timezone.now()
        classe = context["classe"]
        compiti = classe.compiti.all()
        context["compiti"] = compiti.filter(data__gte=oggi)
        context["compiti_passati"] = compiti.filter(data__lte=oggi)

        # creazione link invito alla classe
        classe_uuid = context["classe"].uuid
        invito_url = "{}://{}{}".format(
            self.request.scheme,
            self.request.get_host(),
            reverse("invito-classe", args=(classe_uuid,)),
        )
        context["invito_url"] = invito_url

        if is_genitore(self.request.user):
            # eventi = classe.eventi.all()
            inviti = self.request.user.genitore.inviti.all().filter(
                evento__classe=classe
            )
            # lista di tuple con evento e invito associato
            context["eventi_inviti"] = zip((invito.evento for invito in inviti), inviti)

        return context
