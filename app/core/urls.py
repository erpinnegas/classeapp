"""
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.contrib.auth import views as auth_views
from .views import *
from api import urls as api_urls

urlpatterns = [
    path("", home_view, name="home"),
    path("logout/", logout, name="logout"),
    path("profile/edit", UserUpdateView.as_view(), name="modifica-profilo"),
    path("classe/<uuid:uuid>/", invito_classe, name="invito-classe"),
    path("classe/trova/", ricerca_classe, name="ricerca-classe"),
    path("classe/<int:pk>/", ClasseDetail.as_view(), name="dettagli-classe"),
    path(
        "password/",
        auth_views.PasswordChangeView.as_view(
            template_name="core/password_change.html", success_url="/"
        ),
        name="dettagli-classe",
    ),
]
