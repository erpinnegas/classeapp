from django import forms
from django.contrib.auth.forms import (
    UserCreationForm,
    UserChangeForm,
    AuthenticationForm,
)
from django.db import transaction

from .models import User, Insegnante, Genitore

# Form che riguardano solo autenticazione utenti (istanze di User)


class AuthenticationFormInsegnante(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not hasattr(user, "insegnante"):
            raise forms.ValidationError(
                "Questo account non appartiene ad un insegnante", code=""
            )


class AuthenticationFormGenitore(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not hasattr(user, "genitore"):
            raise forms.ValidationError(
                "Questo account non appartiene ad un genitore", code=""
            )


class CustomUserCreationForm(UserCreationForm):
    data_nascita = forms.DateTimeField(
        input_formats=["%m/%d/%Y"],
        widget=forms.DateInput(
            attrs={
                "class": "form-control datetimepicker-input",
                "data-target": "#data-nascita-picker",
            }
        ),
    )

    class Meta:
        model = User
        fields = ("email", "first_name", "last_name", "data_nascita")


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ("email", "first_name", "last_name", "data_nascita")


class UuidForm(forms.Form):
    """ Form che rappresenta un singolo UUID inserito da un utente per ricercare una classe
    """

    uuid = forms.UUIDField(label="Codice di invito alla classe", required=True)
