from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import *


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_display = ("email", "is_staff", "is_active")
    list_filter = ("email", "is_staff", "is_active")
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        ("Personal info", {"fields": ("first_name", "last_name", "data_nascita")}),
        ("Permissions", {"fields": ("is_staff", "is_active", "is_insegnante")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "first_name",
                    "last_name",
                    "data_nascita",
                    "password1",
                    "password2",
                    "is_staff",
                    "is_active",
                    "is_insegnante",
                ),
            },
        ),
    )
    search_fields = ("email",)
    ordering = ("email",)


class ClasseAdmin(admin.ModelAdmin):
    readonly_fields = ("uuid",)


admin.site.register(User, CustomUserAdmin)
admin.site.register(Genitore)
admin.site.register(Insegnante)
admin.site.register(Alunno)
admin.site.register(Classe, ClasseAdmin)
admin.site.register(Compito)
admin.site.register(Evento)
admin.site.register(InvitoEvento)
admin.site.register(Colletta)
admin.site.register(InvitoColletta)
