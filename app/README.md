# Installazione ed esecuzione del development server

E' fortemente consigliato creare un `virtualenv` per installare le dipendenze del progetto ed eseguirlo. 
Questo serve ad evitare che le dipendenze vengano installate system-wide.

Il modo piu' basilare per farlo e' utilizzando il pacchetto python `virtualenv` (installabile tramite `pip install virtualenv`),
e creandone uno tramite `python -m venv NOME_VIRTUALENV`. Questo comando creera' una cartella con il nome specificato 
che conterra' i propri binari di `python` e `pip`. 

Per attivarlo e' sufficiente entrare nella cartella del virtualenv ed eseguire il comando `source ./bin/activate`, che impostera'
le variabili d'ambiente necessarie, e dovrebbe anche aggiungere al prompt della propria shell un prefisso con il nome del 
virtualenv.
Per uscire dall'ambiente virtuale e' sufficiente il comando `deactivate`.

Una volta creato ed attivato l'ambiente virtuale, eseguire 
	
	git clone https://bitbucket.org/erpinnegas/classeapp.git

nella cartella desiderata (tipicamente quella del virtualenv appena creato).

Nella radice del repository e' presente il file `requirements.txt` che specifica tutte le dipendenze del progetto.
Per installarle tutte, assicurarsi di aver attivato l'ambiente virtuale e digitare il comando 

    pip install -r requirements.txt

Una volta finito il processo di installazione, si avra' un virtualenv con un'installazione funzionante di Django. 
Tuttavia, il database di cui si serve l'applicazione non sara' inizializzato; per farlo, si utilizzera' lo script di Django
`manage.py` presente nella radice di questo repository per eseguire le migrazioni.

    ./manage.py migrate

A questo punto si e' pronti ad eseguire il server di sviluppo. Questo avviene sempre tramite lo script `manage.py`:

    ./manage.py runserver

E' installato anche un pacchetto che estende le funzionalita' di debug di Django. Per utilizzarlo e' sufficiente avviare
il server utilizzando

    ./manage.py runserver_plus

