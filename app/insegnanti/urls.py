"""
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import (
    SignupInsegnante,
    LoginInsegnante,
    ClasseCreate,
    ajax_mostra_compito_form,
    AggiungiCompito,
    ClasseAggiungiInsegnante,
)
from core.views import home_view

urlpatterns = [
    path("signup/insegnante/", SignupInsegnante.as_view(), name="signup-insegnante"),
    path("login/insegnante/", LoginInsegnante.as_view(), name="login-insegnante"),
    path("home/insegnante/", home_view, name="home-insegnante"),
    path("classe/create", ClasseCreate.as_view(), name="aggiungi-classe"),
    path(
        "insegnante/classe/<int:classe_pk>/aggiungi-compito/",
        AggiungiCompito.as_view(),
        name="aggiungi-compito",
    ),
    path(
        "classe/<uuid:uuid>/conferma-insegnante",
        ClasseAggiungiInsegnante.as_view(),
        name="classe_aggiungi_insegnante",
    ),
    path(
        "ajax/forms/classe/<int:classe_pk>/aggiungicompito/",
        ajax_mostra_compito_form,
        name="ajax-compito-form",
    ),
]
