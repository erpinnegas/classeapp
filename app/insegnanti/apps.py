from django.apps import AppConfig


class InsegnantiConfig(AppConfig):
    name = "insegnanti"
