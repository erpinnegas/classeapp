from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth import views as auth_views, authenticate, login
from django.views.generic import FormView, TemplateView, DetailView, CreateView
from django import views
from django.utils import timezone
from django.urls import reverse_lazy, reverse

from .forms import *
from core.forms import CustomUserCreationForm, AuthenticationFormInsegnante
from core.models import Insegnante, Classe

LOGIN_URL_INSEGNANTE = reverse_lazy("insegnanti:login-insegnante")

# Create your views here.


class InsegnanteRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):
    """ Classe da comporre (aggiungere in testa alla lista di classi di 
        eredita') per ottenere una View che autorizza solo gli insegnanti 
        autenticati.
    """

    login_url = LOGIN_URL_INSEGNANTE

    def test_func(self):
        return hasattr(self.request.user, "insegnante")


def insegnante_required(view):
    def newview(request, *args, **kwargs):
        if not hasattr(request.user, "insegnante"):
            raise PermissionDenied
        return view(request, *args, **kwargs)

    return newview


class SignupInsegnante(FormView):
    form_class = CustomUserCreationForm
    template_name = "insegnanti/signup_insegnante.html"

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
            insegnante = Insegnante.objects.create(user=self.object)
            insegnante.save()
        return redirect(LOGIN_URL_INSEGNANTE)


class LoginInsegnante(views.View):
    errors = None

    def __init__(self, *args, **kwargs):
        super(LoginInsegnante, self).__init__(*args, **kwargs)
        if "errors" in kwargs:
            self.errors = kwargs["errors"]

    def get(self, request):
        form = AuthenticationFormInsegnante()
        return render(
            request,
            "insegnanti/login_insegnante.html",
            {"form": form, "errors": self.errors},
        )

    def post(self, request):
        form = AuthenticationFormInsegnante(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("insegnanti:home-insegnante")
        return render(request, "insegnanti/login_insegnante.html", {"form": form})


class ClasseCreate(InsegnanteRequiredMixin, CreateView):
    model = Classe
    fields = ["anno", "sezione", "anno_inizio"]
    template_name = "core/classe_create.html"

    def form_valid(self, form):
        form.instance.insegnanti.add(request.user.insegnante)
        self.object = form.save()
        return super(CreateView, self).form_valid(form)


@insegnante_required
@login_required
def ajax_mostra_compito_form(request, classe_pk):
    if not request.is_ajax():
        raise PermissionDenied
    if request.method == "GET":
        classe = Classe.objects.get(pk=classe_pk)
        form = CompitoForm()
        return render(
            request,
            "insegnanti/compito_form.html",
            {"form": form, "classe_pk": classe_pk, "classe": classe},
        )


class AggiungiCompito(InsegnanteRequiredMixin, CreateView):
    model = Compito
    form_class = CompitoForm
    template_name = "insegnanti/aggiungi_compito.html"

    def form_valid(self, form):
        context = self.get_context_data()
        classe_pk, classe = context["classe_pk"], context["classe"]
        compito = form.save(commit=False)
        compito.classe = classe
        compito.creatore = self.request.user.insegnante
        compito.save()
        return redirect(reverse("dettagli-classe", kwargs={"pk": classe_pk}))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        classe_pk = self.kwargs.get("classe_pk")
        context["classe_pk"] = classe_pk
        context["classe"] = get_object_or_404(Classe, pk=classe_pk)
        self.context = context
        return context


class ClasseAggiungiInsegnante(InsegnanteRequiredMixin, views.View):
    def post(self, request, uuid):
        classe = get_object_or_404(Classe, uuid=uuid)
        classe.insegnanti.add(request.user.insegnante)
        return redirect(reverse("dettagli-classe", kwargs={"pk": classe.pk}))

    def get(self, request, uuid):
        classe = get_object_or_404(Classe, uuid=uuid)
        return render(
            request,
            "insegnanti/conferma_invito.html",
            context={"classe": classe, "classe_uuid": str(classe.uuid)},
        )
