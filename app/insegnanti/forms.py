from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction

from core.models import *


class InsegnanteForm(forms.ModelForm):
    class Meta:
        model = Insegnante
        exclude = ["user", "scuola"]


class ClasseForm(forms.ModelForm):
    class Meta:
        model = Classe
        exclude = ["scuola"]


class CompitoForm(forms.ModelForm):
    data = forms.DateField(
        input_formats=["%d/%m/%Y"],
        widget=forms.DateInput(
            attrs={
                "class": "form-control datetimepicker-input",
                "data-target": "#data-compito-picker",
            }
        ),
    )

    class Meta:
        model = Compito
        exclude = ["classe", "creatore"]
