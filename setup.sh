#!/bin/sh

echo Questo script creera\' due macchine, di nome "worker1" e "worker2"
echo Poi fara\' il build del progetto, e inviera\' le immagini cosi\' create
echo alle suddette macchine.
echo
echo Premi Ctrl+C entro 4 secondi per annullare...
sleep 4

make create_machines
make start_machines
make send_to_machines


echo
echo
echo Macchine "worker1" e "worker2" create.
echo Adesso puoi far partire lo swarm con:
echo
echo make swarm_init
echo make swarm_up
echo
echo
echo E troverai il sito su 192.168.99.1:1337
echo
echo Per visualizzare lo stato dello swarm, esegui
echo make visualizer_up
echo e troverai l\'interfaccia su localhost:5555
echo
echo "(per spegnere il visualizzatore: make visualizer_down)"
echo
echo
echo Per spegnere lo swarm:
echo make swarm_down
echo
