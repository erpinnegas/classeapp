# Nome da passare in input ai comandi di docker stack
STACK_NAME:="mystack"


# Compila tutte le immagini dei servizi necessari
build:
	docker-compose -f docker-compose.prod.yml build

logs:
	docker-compose -f docker-compose.prod.yml logs

#####################################################################################
####### COMANDI NON SWARM
#######
####### Fanno uso solamente di docker-compose
####### Si possono usare per vedere se tutto funziona in una configurazione semplice
#####################################################################################


# fa partire un'istanza di tutti i servizi, senza repliche
run: shutdown build
	docker-compose -f docker-compose.prod.yml up -d

# Spegne tutti i servizi con docker-compose
shutdown:
	docker-compose -f docker-compose.prod.yml down -v


#####################################################################################
####### COMANDI MODALITA' SWARM
#######
####### Da non mescolare con quelli non-swarm
#####################################################################################

swarm_up: build 
	docker stack deploy --compose-file docker-compose.prod.yml $(STACK_NAME)

swarm_init: start_machines
	docker swarm init --advertise-addr 192.168.99.1 || exit 0
	docker-machine ssh worker1 "$$(docker swarm join-token worker | grep -- '--token') "
	docker-machine ssh worker2 "$$(docker swarm join-token worker | grep -- '--token') "


# Rimuove i worker dallo swarm, stop e rimozione delle vm, si rimuove lo stack e si elimina lo swarm (leave da parte del leader node). Alla fine, non ci saranno più servizi (docker service ps mystack_) e container (docker container ls).
swarm_down:
	docker stack rm mystack
	docker-machine ssh worker1 'docker swarm leave'
	docker-machine ssh worker2 'docker swarm leave'
	#docker-machine stop worker1 & docker-machine stop worker2
	#(echo 'y' | docker-machine rm worker1) & (echo 'y' |docker-machine rm worker2)
	docker swarm leave --force

								  

##############################################################################
######## MANDARE IMMAGINE ALLA DOCKER-MACHINE
##############################################################################

send_to_machines: build
	docker save django_app my_nginx | docker-machine ssh worker1 "docker load"
	docker save django_app my_nginx | docker-machine ssh worker2 "docker load"

create_machines:
	docker-machine create worker1 || exit 0
	docker-machine create worker2 || exit 0

start_machines:
	docker-machine start worker1 || exit 0
	docker-machine start worker2 || exit 0

stop_machines:
	docker-machine stop worker1
	docker-machine stop worker2

######################################
######## VISUALIZER
######################################


visualizer_up:
	docker run -it -d --name myvisualizer -p "5555:8080" -v /var/run/docker.sock:/var/run/docker.sock dockersamples/visualizer

visualizer_down:
	docker stop myvisualizer
